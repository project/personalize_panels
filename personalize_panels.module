<?php

/**
 * @file
 * Main module hooks for the integrating personalization with Panels.
 */

/**
 *  Implements hook_menu().
 */
function personalize_panels_menu()  {
  $items = array();
  $items['admin/structure/personalize/variations/personalize-panels'] = array(
    'title' => 'Panels',
    'description' => 'Create personalized panels.',
    'page callback' => 'personalize_panels_list',
    'access callback' => 'user_access',
    'access arguments' => array('manage personalized content'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'personalize_panels.admin.inc',
  );
  $items['admin/structure/personalize/variations/personalize-panels/manage/%personalize_panel'] = array(
    'title' => 'Edit Personalized Panel',
    'description' => 'Create personalized panels.',
    'page callback' => 'personalize_panels_list',
    'access callback' => 'user_access',
    'access arguments' => array('manage personalized content'),
    'file' => 'personalize_panels.admin.inc',
  );
  $items['admin/structure/personalize/variations/personalize-panels/manage/%personalize_panel/delete'] = array(
    'title' => 'Delete Personalized Panel',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('personalize_panels_variation_set_delete', 6),
    'access arguments' => array('manage personalized content'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'personalize_panels.forms.inc',
  );
  return $items;
}

/**
 * Implements hook_theme().
 */
function personalize_panels_theme($existing, $type, $theme, $path) {
  return array(
    'personalize_panels_form_variants' => array(
      'render element' => 'element',
      'file' => 'theme/theme.inc',
    ),
  );
}

/**
 * Implements hook_ctools_plugin_directory().
 */
function personalize_panels_ctools_plugin_directory($module, $plugin) {
  if ($plugin != 'content_types') {
    return;
  }
  return 'plugins/content_types';
}

/**
 * Implements hook_ctools_plugin_api().
 */
function personalize_panels_ctools_plugin_api($owner, $api) {
  if ($owner == 'personalize' && $api == 'personalize') {
    return array('version' => 1);
  }
}

/**
 * Implements hook_personalize_option_set_type().
 */
function personalize_panels_personalize_option_set_type() {
  return array(
    'panel_pane' => array()
  );
}

/**
 * Implements hook_personalize_create_new_links().
 *
 * Since you only add personalization panes in the Panels interface it doesn't
 * make a ton of sense to add a link like this here, but the Personalize module
 * says you should.
 */
function personalize_panels_personalize_create_new_links() {
  return array(
    array(
      'title' => 'Personalized Panel pane',
      'path' => 'admin/structure/pages',
    )
  );
}

/**
 * Implements hook_personalize_edit_link().
 */
function personalize_panels_personalize_edit_link($option_set) {
  if ($option_set->plugin != 'panel_pane' || empty($option_set->data['edit_url'])) {
    return NULL;
  }
  return $option_set->data['edit_url'];
}

/**
 * Implements hook_personalize_delete_link().
 */
function personalize_panels_personalize_delete_link($option_set) {
  if ($option_set->plugin != 'panel_pane') {
    return NULL;
  }
  return "admin/structure/personalize/variations/personalize-panels/manage/{$option_set->osid}/delete";
}

/**
 * Implements hook_panels_pre_render().
 */
function personalize_panels_panels_pre_render($panels_display, $renderer) {
  $content = &$panels_display->content;

  foreach ($content as $pane) {
    if ($pane->type != 'personalize_pane') {
      continue;
    }

    // We need to turn off the two panes that are part of this test, so that
    // only the Lift picked one appears.
    $option_set = personalize_option_set_load($pane->configuration['osid']);
    if (!$option_set) {
      continue;
    }
    
    foreach ($option_set->options as $option) {
      foreach ($content as $key => &$value) {
        if ($option['panels_pane_pid'] == $value->uuid) {
          $value->shown = 0;
          break;
        }
      }
    }
    break;
  }
}

/**
 * Loads a personalized panel by its osid.
 *
 * @param $osid
 *   The osid of the option set.
 * @return The loaded option set or NULL no option set exists.
 */
function personalize_panel_load($osid) {
  $os = personalize_option_set_load($osid);
  if (!$os || $os->plugin != 'panel_pane') {
    return NULL;
  }
  return $os;
}

/**
 *  Implements hook_panels_pane_delete().
 */
function personalize_panels_panels_pane_delete($pids) {
  foreach ($pids as $pid) {
    $pane = db_select('panels_pane', 'pp')
      ->fields('pp')
      ->condition('pp.pid', $pid, '=')
      ->execute()->fetchAssoc();
    if (empty($pane['type']) || $pane['type'] != 'personalize_pane')  {
      continue;
    }
    $conf = unserialize($pane['configuration']);
    personalize_option_set_delete($conf['osid']);
  }
}
