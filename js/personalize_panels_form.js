/**
 * @file
 * Javascript for the Personalization Panel admin form.
 */

(function ($) {
  Drupal.behaviors.personalize_panels_form = {
    
    attach: function (context, settings) {
      const $variantSelect = $('.variant-selector', context), self = this;
      $options = $('option', $variantSelect);
      this.process($variantSelect, $options);

      $variantSelect.change(function(e) {
        $options.attr('disabled', false);
        self.process($variantSelect, $options);
      });
    },
    /**
     * Process the current state of all selects in the form.
     *
     * @param jQuery Element $elements
     *  The select elements to process.
     * @param jQuery Element $options
     *  The option elements to process.
     */
    process: function($elements, $options) {
      let selectedOptions = [], selectedValues = [];
      $elements.each(function(i, element) {
        selectedOptions[i] = $(element).find('option:selected').get(0);
        selectedValues[i] = selectedOptions[i].value;
      });

      $options.each(function(i, option) {
        if (selectedValues.indexOf(option.value) < 0 || selectedOptions.indexOf(option) >= 0) {
          return true;
        }
        option.disabled = true;
      });
    }

  };

}(jQuery));